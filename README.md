# crystal_poc

TODO: Write a description here

## Installation

TODO: Write installation instructions here

## Usage

TODO: Write usage instructions here

## Development

TODO: Write development instructions here

## Contributing

1. Fork it ( https://github.com/paulopatto/crystal_poc/fork )
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create a new Pull Request

## Contributors

- [paulopatto](https://github.com/paulopatto) Paulo Patto - creator, maintainer

## Source

Read more in https://blog.codeship.com/crystal-from-a-rubyists-perspective/
Watch Rails Conf talk https://youtu.be/gHoNNdeedf0
